## O:

Today I learnt about web development, the RESTful interfaces and the use of Springboot.

## R：

I don't feel very familiar with Springboot.

## I：

Through the study of RESTful style interface, understand the six HTTP request methods, commonly used GET, POST, DELETE, PUT. also understand the meaning of the different state code representatives, the common state code 2xx.4xx and 5xx corresponding to the meaning to remember.

## D:

RESTful style interfaces allow one to visualise the resources and structure of the request when reading the URL, making it easy to understand the request. I will try to use RESTful interfaces in the future as well