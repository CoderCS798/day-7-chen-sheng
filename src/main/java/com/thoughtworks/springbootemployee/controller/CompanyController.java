package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repo.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final List<Company> companies = new ArrayList<>();
    private final AtomicLong atomicId = new AtomicLong(0);

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        company.setId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    @GetMapping("/{companyId}")
    public Company findCompanyById(@PathVariable Long companyId) {
        return companies.stream()
                .filter(company -> companyId.equals(company.getId()))
                .findFirst()
                .orElse(null);
    }

    @GetMapping
    public List<Company> findAllCompanies() {
        return companies;
    }

    @PutMapping("/{companyId}")
    public Company updateCompanyName(@PathVariable Long companyId, @RequestBody Employee company) {
        Company updatedCompany = findCompanyById(companyId);
        updatedCompany.setName(company.getName());
        return updatedCompany;
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompanyById(@PathVariable Long companyId) {
        companies.remove(findCompanyById(companyId));
    }
    @GetMapping(params = {"page", "size"})
    public List<Company> queryCompanies(@RequestParam Integer page, @RequestParam Integer size) {
        return companies.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesOfCompany(@PathVariable Long companyId) {
        return EmployeeRepository.getEmployees().stream()
                .filter(employee -> companyId.equals(employee.getCompanyId()))
                .collect(Collectors.toList());
    }
}
