package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repo.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee create(@RequestBody Employee employee) {

        return EmployeeRepository.addEmployee(employee);
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return EmployeeRepository.getEmployees();
    }

    @GetMapping("/{employeeId}")
    public Employee getEmployeeById(@PathVariable Long employeeId) {
        return EmployeeRepository.getEmployeeById(employeeId);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> findEmployeesByGender(@RequestParam String gender) {
        return EmployeeRepository.findEmployeesByGender(gender);
    }

    @PutMapping("/{employeeId}")
    public Employee updateAgeAndSalary(@PathVariable Long employeeId, @RequestBody Employee employee) {

        return EmployeeRepository.updateAgeAndSalary(employeeId,employee);
    }

    @DeleteMapping("/{employeeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long employeeId) {
        EmployeeRepository.deleteEmployeeById(employeeId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> queryEmployees(@RequestParam Integer page, @RequestParam Integer size) {
        return EmployeeRepository.queryEmployees(page, size);
    }

}
