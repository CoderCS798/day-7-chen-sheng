package com.thoughtworks.springbootemployee.repo;

import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private static final List<Employee> employees = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    public static Employee addEmployee(Employee employee){
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public static List<Employee> getEmployees(){
        return employees;
    }
    public static Employee getEmployeeById(Long id){
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .get();
    }
    public static List<Employee> findEmployeesByGender(String gender){
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }
    public static Employee updateAgeAndSalary(Long id,Employee employee){
        Employee updatedEmployee = getEmployeeById(id);
        updatedEmployee.setAge(employee.getAge());
        updatedEmployee.setSalary(employee.getSalary());
        return updatedEmployee;
    }
    public static void deleteEmployeeById(Long id){
        employees.remove(getEmployeeById(id));
    }
    public static List<Employee>queryEmployees(Integer page,Integer size){
        return employees.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

}
